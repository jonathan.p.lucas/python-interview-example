# Python test

This directory contains the following files.

* `README.md`: this file
* `codeclimate.json`: a JSON format file containing a list of records.


## Question 1

Write a Python script to count the number of many records  in `codeclimate.json``

## Question 2

Update the script to output a JSON file containing all records where the path does not include "3rd_party".

How many records are there in the filtered result?

## Question 3

Modify the script so that it takes command line arguments allowing both the input file and the exclude path to be set.

## Question 4

It is sometimes useful to provide information about the progress of the script. How would you implement this?

## Question 5

How would you test this script?

## Question 6

Modify the command line argument to take multiple exclude paths.

